from mininet.node import Controller
from mininet.log import setLogLevel , info
from mn_wifi.net import Mininet_wifi
from mn_wifi.node import OVSKernelAP
from mn_wifi.cli import CLI_wifi

def topology():

    net = Mininet_wifi( controller=Controller, accessPoint=OVSKernelAP )

    info("*** Creating nodes\n")
    ap1 = net.addAccessPoint( 'ap1', ssid= 'handover', mode= 'g', channel= '1', position='12,25,0', range='15')
    ap2 = net.addAccessPoint( 'ap2', ssid= 'handover2', mode= 'g', channel= '6', position='32,25,0', range='15')
    sta1 = net.addStation( 'sta1', mac='00:00:00:00:00:01', ip='10.0.0.1/8', position='10,25,0', range='5',speed='1' )
    sta2 = net.addStation( 'sta2', mac='00:00:00:00:00:02', ip='10.0.0.2/8', position='10,23,0', range='5' )
    c1 = net.addController( 'c1', controller=Controller )

    net.configureWifiNodes( )
    net.plotGraph(max_x=50, max_y=50)
    info("*** Enabling association control (AP)\n")

    info("*** Creating links and associations\n")
    net.addLink( ap1, ap2 )
    net.addLink( sta2, ap1 )
    net.addLink( sta1, ap1 )

    net.startMobility(time=0 , AC='ssf')
    net.mobility(sta1,'start',time=40, position='10,25,0')
    net.mobility(sta1,'stop', time=69, position='49,25,0')
    net.stopMobility(time=70)

    info("*** Starting network\n")
    net.build()
    c1.start()
    ap1.start( [c1] )
    ap2.start( [c1] )

    info("*** Running CLI\n")
    CLI_wifi( net )

    info("*** Stopping network\n")
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    topology()

