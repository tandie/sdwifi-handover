from mininet.node import Controller
from mininet.log import setLogLevel , info
from mn_wifi.net import Mininet_wifi
from mn_wifi.node import OVSKernelAP
from mn_wifi.cli import CLI_wifi

def topology():

    net = Mininet_wifi( controller=Controller, accessPoint=OVSKernelAP )

    info("*** Creating nodes\n")
    ap1 = net.addAccessPoint( 'ap1', ssid= 'ssid-ap1', mode= 'g', channel= '1', position='25,20,0', range='25' )
    sta1 = net.addStation( 'sta1', mac='00:00:00:00:00:01', ip='10.0.0.1/8', position='15,25,0', range='10',speed='1.5' )
    sta2 = net.addStation( 'sta2', mac='00:00:00:00:00:02', ip='10.0.0.2/8', position='45,25,0', range='10' )
    c1 = net.addController( 'c1', controller=Controller )

    net.configureWifiNodes( )
    net.plotGraph(max_x=50, max_y=50)
    info("*** Enabling association control (AP)\n")

    info("*** Creating links and associations\n")
    net.addLink( ap1, sta1 )
    net.addLink( ap1, sta2 )

    net.startMobility(time=0 , AC='ssf')
    net.mobility(sta1,'start',time=40, position='15,25,0')
    net.mobility(sta1,'stop', time=69, position=' 46,25,0')
    net.stopMobility(time=70)

    info("*** Starting network")
    net.build()
    c1.start()
    ap1.start( [c1] )

    info("*** Running CLI\n")
    CLI_wifi( net )

    info("*** Stopping network\n")
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    topology()
